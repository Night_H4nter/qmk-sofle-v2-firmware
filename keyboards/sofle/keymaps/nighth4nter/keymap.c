#include QMK_KEYBOARD_H



enum sofle_layers {
    _us,
    _SYMBOLS,
    _misc
};



//left hand home row mods on default layer
#define HOME_CAPS LCTL_T(KC_ESC)
#define HOME_S LALT_T(KC_S)
#define HOME_D LGUI_T(KC_D)
#define HOME_F LSFT_T(KC_F)

// Right-hand home row mods on default layer
#define HOME_J RSFT_T(KC_J)
#define HOME_K RGUI_T(KC_K)
#define HOME_L LALT_T(KC_L)
#define HOME_ENT RCTL_T(KC_ENT)

//tap dance declaration
enum {
    TD_SUPER_ESC,
};

qk_tap_dance_action_t tap_dance_actions[] = {
    // Tap once for super and twice for esc
    [TD_SUPER_ESC] = ACTION_TAP_DANCE_DOUBLE(KC_LGUI, KC_ESC),
};

uint16_t get_tapping_term(uint16_t keycode, keyrecord_t *record) {
    switch (keycode) {
        case TD(TD_SUPER_ESC):
        case RGUI_T(KC_ENT):
            return 300;
        default:
            return 200;
    }
}

uint16_t get_retro_taping(uint16_t keycode, keyrecord_t *record) {
    switch (keycode) {
        case TD(TD_SUPER_ESC):
        case RGUI_T(KC_ENT):
            return true;
        default:
            return false;
    }
}

bool get_tapping_force_hold(uint16_t keycode, keyrecord_t *record) {
    switch (keycode) {
        case LGUI_T(KC_ESC):
            return true;
        default:
            return false;
    }
}



const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
    [_us] = LAYOUT( \
      KC_GRV,           KC_1,   KC_2,    KC_3,    KC_4,     KC_5,                                     KC_6,    KC_7,     KC_8,    KC_9,   KC_0,      KC_DEL,            \
      KC_TAB,           KC_Q,   KC_W,    KC_E,    KC_R,     KC_T,                                     KC_Y,    KC_U,     KC_I,    KC_O,   KC_P,      KC_BSLS,           \
      HOME_CAPS,        KC_A,   HOME_S,  HOME_D,  HOME_F,   KC_G,                                     KC_H,    HOME_J,   HOME_K,  HOME_L, KC_SCLN,   HOME_ENT,          \
      OSM(MOD_LSFT),    KC_Z,   KC_X,    KC_C,    KC_V,     KC_B,   KC_MUTE,                XXXXXXX,  KC_N,    KC_M,     KC_COMM, KC_DOT, KC_QUOT,   OSM(MOD_RSFT),     \
                              MO(_misc), XXXXXXX, KC_RBRC, KC_SPC, OSL(_SYMBOLS),     OSL(_SYMBOLS),  KC_BSPC, KC_LBRC, XXXXXXX, MO(_misc)                              \
    ),
    
    [_SYMBOLS] = LAYOUT( \
      KC_F1,   KC_F2,   KC_F3,   KC_F4,   KC_F5,    KC_F6,                                    KC_F7,    KC_F8,   KC_F9,   KC_F10,  KC_F11,   TO(_us),                   \
      KC_GRV,  KC_1,    KC_2,    KC_3,    KC_4,     KC_5,                                     KC_6,     KC_7,    KC_8,    KC_9,    KC_0,     KC_DEL,                    \
      KC_CAPS, KC_DQT,  KC_COLN, KC_LCBR, KC_DOT,   KC_UNDS,                                  KC_MINS,  KC_COMM, KC_RCBR, KC_SCLN, KC_QUOT,  RGUI_T(KC_ENT),            \
      _______, KC_BSLS, KC_LPRN, KC_LBRC, KC_PLUS,  KC_AT,  _______,                 _______, KC_EXLM,  KC_EQL,  KC_SCLN, KC_RPRN, KC_SLASH, _______,                   \
                        TO(_us), KC_LALT, KC_LCTL, KC_SPC, KC_NO,                 KC_NO,  KC_BSPC, KC_RCTL, KC_LALT, TO(_us)                                      \
    ),
    
    [_misc] = LAYOUT( \
      KC_F1,   KC_F2,   KC_F3,   KC_F4,   KC_F5, KC_F6,                         KC_F7,   KC_F8,   KC_F9,  KC_F10,  KC_F11, KC_F12,                                      \
      _______,  KC_INS,  KC_PSCR,   KC_APP,  XXXXXXX, XXXXXXX,                        KC_PGUP, XXXXXXX,   KC_DEL, XXXXXXX,XXXXXXX, KC_BSPC,                             \
      TO(_us), KC_LALT,  KC_LCTL,  KC_LSFT,  XXXXXXX, KC_CAPS,                       KC_PGDN,  KC_LEFT, KC_DOWN, KC_UP,  KC_RGHT, RGUI_T(KC_ENT),                       \
      _______,KC_UNDO, KC_CUT, KC_COPY, KC_PASTE, XXXXXXX,  _______,       _______,  XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,   XXXXXXX, _______,                            \
                               TO(_us),KC_LALT,KC_LCTL, KC_SPC, OSL(_SYMBOLS),     OSL(_SYMBOLS),  KC_BSPC, KC_RCTL, KC_LALT, TO(_us)                                 \
    ),
};



#ifdef OLED_ENABLE

static void render_logo(void) {
    static const char PROGMEM qmk_logo[] = {
        0x80,0x81,0x82,0x83,0x84,0x85,0x86,0x87,0x88,0x89,0x8a,0x8b,0x8c,0x8d,0x8e,0x8f,0x90,0x91,0x92,0x93,0x94,
        0xa0,0xa1,0xa2,0xa3,0xa4,0xa5,0xa6,0xa7,0xa8,0xa9,0xaa,0xab,0xac,0xad,0xae,0xaf,0xb0,0xb1,0xb2,0xb3,0xb4,
        0xc0,0xc1,0xc2,0xc3,0xc4,0xc5,0xc6,0xc7,0xc8,0xc9,0xca,0xcb,0xcc,0xcd,0xce,0xcf,0xd0,0xd1,0xd2,0xd3,0xd4,0
    };

    oled_write_P(qmk_logo, false);
}

/* static void print_status_narrow(void) { */
/*     // Print current mode */
/*     oled_write_P(PSTR("\n\n"), false); */
/*     oled_write_ln_P(PSTR("MODE"), false); */
/*     oled_write_ln_P(PSTR(""), false); */
/*     if (keymap_config.swap_lctl_lgui) { */
/*         oled_write_ln_P(PSTR("MAC"), false); */
/*     } else { */
/*         oled_write_ln_P(PSTR("WIN"), false); */
/*     } */

/*     switch (get_highest_layer(default_layer_state)) { */
/*         case _QWERTY: */
/*             oled_write_ln_P(PSTR("Qwrt"), false); */
/*             break; */
/*         case _COLEMAK: */
/*             oled_write_ln_P(PSTR("Clmk"), false); */
/*             break; */
/*         default: */
/*             oled_write_P(PSTR("Undef"), false); */
/*     } */
/*     oled_write_P(PSTR("\n\n"), false); */
/*     // Print current layer */
/*     oled_write_ln_P(PSTR("LAYER"), false); */
/*     switch (get_highest_layer(layer_state)) { */
/*         case _COLEMAK: */
/*         case _QWERTY: */
/*             oled_write_P(PSTR("Base\n"), false); */
/*             break; */
/*         case _RAISE: */
/*             oled_write_P(PSTR("Raise"), false); */
/*             break; */
/*         case _LOWER: */
/*             oled_write_P(PSTR("Lower"), false); */
/*             break; */
/*         case _ADJUST: */
/*             oled_write_P(PSTR("Adj\n"), false); */
/*             break; */
/*         default: */
/*             oled_write_ln_P(PSTR("Undef"), false); */
/*     } */
/*     oled_write_P(PSTR("\n\n"), false); */
/*     led_t led_usb_state = host_keyboard_led_state(); */
/*     oled_write_ln_P(PSTR("CPSLK"), led_usb_state.caps_lock); */
/* } */

/* oled_rotation_t oled_init_user(oled_rotation_t rotation) { */
/*     if (is_keyboard_master()) { */
/*         return OLED_ROTATION_270; */
/*     } */
/*     return rotation; */
/* } */

bool oled_task_user(void) {
    /* if (is_keyboard_master()) { */
    /*     print_status_narrow(); */
    /* } else { */
        render_logo();
    /* } */
    return false;
}

#endif



/* layer_state_t layer_state_set_user(layer_state_t state) { */
/*     return update_tri_layer_state(state, _LOWER, _RAISE, _ADJUST); */
/* } */

/* bool process_record_user(uint16_t keycode, keyrecord_t *record) { */
/*     switch (keycode) { */
/*     } */
/*     return true; */
/* } */

#ifdef ENCODER_ENABLE

bool encoder_update_user(uint8_t index, bool clockwise) {
    if (index == 0) {
        if (clockwise) {
            tap_code(KC_VOLU);
        } else {
            tap_code(KC_VOLD);
        }
    } else if (index == 1) {
        if (clockwise) {
            tap_code(KC_PGDN);
        } else {
            tap_code(KC_PGUP);
        }
    }
    return true;
}

#endif
